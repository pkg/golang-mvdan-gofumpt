golang-mvdan-gofumpt (0.4.0-1+apertis1) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Wed, 22 Jan 2025 16:36:55 +0100

golang-mvdan-gofumpt (0.4.0-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 10:46:26 +0000

golang-mvdan-gofumpt (0.4.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.4.0
  * Set debian/watch to track released tarballs
  * Use dh-sequence-golang instead of dh-golang and --with=golang
  * Bump Standards-Version to 4.6.2 (no change)
  * debian/rules: Install testdata again to make the tests pass

 -- Anthony Fok <foka@debian.org>  Wed, 25 Jan 2023 13:33:23 -0700

golang-mvdan-gofumpt (0.3.1-1) unstable; urgency=medium

  * New upstream release

 -- Shengjing Zhu <zhsj@debian.org>  Wed, 23 Mar 2022 01:45:53 +0800

golang-mvdan-gofumpt (0.2.0-1) unstable; urgency=medium

  * New upstream release
  * Add golang-github-frankban-quicktest-dev to Build-Depends

 -- Shengjing Zhu <zhsj@debian.org>  Thu, 11 Nov 2021 02:47:13 +0800

golang-mvdan-gofumpt (0.1.1-1) unstable; urgency=medium

  * New upstream release
  * Update Standards-Version to 4.6.0 (no changes)
  * Add Multi-Arch hint

 -- Shengjing Zhu <zhsj@debian.org>  Sat, 09 Oct 2021 23:38:32 +0800

golang-mvdan-gofumpt (0.1.0-1co0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 28 Apr 2021 16:41:11 +0200

golang-mvdan-gofumpt (0.1.0-1) unstable; urgency=medium

  * New upstream release
  * Update Standards-Version to 4.5.1 (no changes)

 -- Shengjing Zhu <zhsj@debian.org>  Wed, 27 Jan 2021 02:12:12 +0800

golang-mvdan-gofumpt (0.0~git20201107.a024667-2) unstable; urgency=medium

  * Drop golang-golang-x-tools-dev from Depends.
    To avoid circular dependencies.

 -- Shengjing Zhu <zhsj@debian.org>  Tue, 17 Nov 2020 12:18:17 +0800

golang-mvdan-gofumpt (0.0~git20201107.a024667-1) unstable; urgency=medium

  * Initial release (Closes: #974930)

 -- Shengjing Zhu <zhsj@debian.org>  Tue, 17 Nov 2020 01:24:12 +0800
